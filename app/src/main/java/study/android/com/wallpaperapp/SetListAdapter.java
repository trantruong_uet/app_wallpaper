package study.android.com.wallpaperapp;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

/**
 * Created by Tran Truong on 3/26/2016.
 */
public class SetListAdapter extends BaseAdapter {

    private Activity context;
    private int layout;
    private String[] title,description,downloadcount,timemodified,photoUrl;
    private int so;

    public SetListAdapter(Activity context, int layout, String[] title, String[] description, String[] downloadcount,String[]timemodified,String[]photoUrl,int so) {
        this.context = context;
        this.layout = layout;
        this.title = title;
        this.description = description;
        this.downloadcount = downloadcount;
        this.timemodified = timemodified;
        this.photoUrl = photoUrl;
        this.so = so;
    }


    @Override
    public int getCount() {
        return so;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SetItem setItem;

        if( convertView == null)
        {
            convertView = context.getLayoutInflater().inflate(layout,null);
            setItem = new SetItem(convertView);
            convertView.setTag(setItem);
        }
        else setItem = (SetItem)convertView.getTag();

        //new DownloadImageTask(setItem.imageView).execute(photoUrl[position]);
        Picasso.with(context).load(photoUrl[position]).resize(100, 100).centerCrop().into(setItem.imageView);
        setItem.headline.setText(title[position]);

        if(description[position].equals("null"))
            setItem.title.setText("");
        else
            setItem.title.setText(description[position]);

        setItem.download.setText(""+downloadcount[position]);
        setItem.time.setText(timemodified[position]);
        return convertView;
    }

    static class SetItem{
        View convertView;
        ImageView imageView;
        TextView headline,title,time,download;
        public SetItem(View convertView){
            imageView = (ImageView) convertView.findViewById(R.id.image_list);
            headline = (TextView) convertView.findViewById(R.id.tv_headline);
            title = (TextView) convertView.findViewById(R.id.tv_title);
            time = (TextView) convertView.findViewById(R.id.tv_time);
            download= (TextView) convertView.findViewById(R.id.tv_list_download);
        }
    }
//    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
//
//        ImageView bmImage;
//        public DownloadImageTask(ImageView bmImage) {
//            this.bmImage = bmImage;
//        }
//
//
//        protected Bitmap doInBackground(String... urls) {
//            String urldisplay = urls[0];
//            Bitmap mIcon11 = null;
//            try {
//                InputStream in = new java.net.URL(urldisplay).openStream();
//                mIcon11 = BitmapFactory.decodeStream(in);
//            } catch (Exception e) {
//                Log.e("Error", e.getMessage());
//                e.printStackTrace();
//            }
//            return mIcon11;
//        }
//
//        protected void onPostExecute(Bitmap result) {
//            bmImage.setImageBitmap(result);
//        }
//    }
}
