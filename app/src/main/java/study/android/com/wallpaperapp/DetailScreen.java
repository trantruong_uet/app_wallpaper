package study.android.com.wallpaperapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.squareup.picasso.Picasso;

import cz.msebera.android.httpclient.Header;

public class DetailScreen extends AppCompatActivity {
    Button btn_back;
    ImageView imv_detail,btn_download;
    TextView tv_detail_title,tv_download;
    private String s;
    private int download;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_screen);
        setview();
        setBtn_back();
        getdatafromintent();
        btn_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setBtn_download();
            }
        });
    }

    private void setview()
    {
        btn_back = (Button)findViewById(R.id.btn_back);
        btn_download = (ImageView) findViewById(R.id.btn_download);
        tv_detail_title = (TextView) findViewById(R.id.tv_detail_title);
        tv_download = (TextView) findViewById(R.id.tv_download);
        imv_detail = (ImageView)findViewById(R.id.image_detail);
    }

    private void setBtn_back()
    {
        btn_back = (Button) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void getdatafromintent()
    {
        Intent intent = getIntent();
        s = (String) intent.getStringExtra("photoUrl");
        String ss = (String) intent.getStringExtra("download");
        String sss = (String) intent.getStringExtra("title");
        download = Integer.valueOf(ss);

        tv_download.setText(ss+"  Downloaded");
        tv_detail_title.setText(sss);
        Picasso.with(DetailScreen.this).load(s).resize(1000, 1575).centerCrop().into(imv_detail);
    }
    private void setBtn_download(){

            AsyncHttpClient client = new AsyncHttpClient();
            RequestParams  params = new RequestParams();
            params.put("photoId", (download + 1));
            client.post(this, "http://mobilewalls-snapsofts.rhcloud.com/mobilewalls/api/update-photo-download", params, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    Toast.makeText(DetailScreen.this,""+(download+1),Toast.LENGTH_LONG).show();

                }
            });

    }
//    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
//
//        ImageView bmImage;
//        public DownloadImageTask(ImageView bmImage) {
//            this.bmImage = bmImage;
//        }
//
//
//        protected Bitmap doInBackground(String... urls) {
//            String urldisplay = urls[0];
//            Bitmap mIcon11 = null;
//            try {
//                InputStream in = new java.net.URL(urldisplay).openStream();
//                mIcon11 = BitmapFactory.decodeStream(in);
//            } catch (Exception e) {
//                Log.e("Error", e.getMessage());
//                e.printStackTrace();
//            }
//            return mIcon11;
//        }
//
//        protected void onPostExecute(Bitmap result) {
//            bmImage.setImageBitmap(result);
//        }
//    }
//    private void setBtn_download()
//    {
//        AsyncHttpClient client = new AsyncHttpClient();
//        final RequestParams params = new RequestParams();
//        client.post("http://mobilewalls-snapsofts.rhcloud.com/mobilewalls/api/update-photo-download", params, new TextHttpResponseHandler() {
//            @Override
//            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
//
//            }
//
//            @Override
//            public void onSuccess(int statusCode, Header[] headers, String responseString) {
//                JSONObject jsonObject = null;
//
//                try {
//                    jsonObject = new JSONObject(responseString);
//                    JSONArray jsonArray = jsonObject.getJSONArray("data");
//
//                    for (int i = 0; i < jsonArray.length(); i++) {
//                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
//
//                        if (jsonObject1.getString("photoUrl").equals(s))
//                            params.put("downloadCount", jsonObject1.getInt("downloadCount") + 1);
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        });
//    }
//    private class GetImages extends AsyncTask<Object, Object, Object> {
//        private String requestUrl, imagename_;
//        private ImageView view;
//        private Bitmap bitmap ;
//        private FileOutputStream fos;
//        private GetImages(String requestUrl, ImageView view, String _imagename_) {
//            this.requestUrl = requestUrl;
//            this.view = view;
//            this.imagename_ = _imagename_ ;
//        }
//
//        @Override
//        protected Object doInBackground(Object... objects) {
//            try {
//                URL url = new URL(requestUrl);
//                URLConnection conn = url.openConnection();
//                bitmap = BitmapFactory.decodeStream(conn.getInputStream());
//            } catch (Exception ex) {
//            }
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(Object o) {
//            if(!ImageStorage.checkifImageExists(imagename_))
//            {
//                view.setImageBitmap(bitmap);
//                ImageStorage.saveToSdCard(bitmap, imagename_);
//            }
//        }
//    }
}
