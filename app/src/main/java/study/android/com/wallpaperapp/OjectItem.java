package study.android.com.wallpaperapp;

/**
 * Created by Tran Truong on 3/26/2016.
 */
public class OjectItem {
    private String headline;
    private String title;
    private String download;

    public OjectItem(String headline, String title, String download) {
        this.headline = headline;
        this.title = title;
        this.download = download;
    }

    public String getHeadline() {
        return headline;
    }

    public String getTitle() {
        return title;
    }

    public String getDownload() {
        return download;
    }
}
