package study.android.com.wallpaperapp;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

/**
 * Created by Tran Truong on 3/26/2016.
 */
public class SetGridAdapter extends BaseAdapter{
    private Activity context;
    private int layout;
    private String[] download;
    private String[] photoUrl;
    private int so;

    public SetGridAdapter(Activity context, int layout, String[] download,String[] photoUrl, int so) {
        this.context = context;
        this.layout = layout;
        this.download = download;
        this.photoUrl = photoUrl;
        this.so = so;
    }

    @Override
    public int getCount() {
        return so;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SetItem setItem;

        if(convertView == null)
        {
            convertView = context.getLayoutInflater().inflate(layout,null);
            setItem = new SetItem(convertView);
            convertView.setTag(setItem);
        }
        else setItem = (SetItem)convertView.getTag();

        //new DownloadImageTask(setItem.imageView).execute(photoUrl[position]);
        Picasso.with(context).load(photoUrl[position]).resize(100,100).centerCrop().into(setItem.imageView);
        setItem.textView.setText(download[position]);
        return convertView;
    }

    static class SetItem{
        View convertView;
        ImageView imageView;
        TextView textView;

        public SetItem(View convertView)
        {
            imageView = (ImageView) convertView.findViewById(R.id.image_grid);
            textView = (TextView) convertView.findViewById(R.id.tv_download);
        }
    }
//    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
//
//        ImageView bmImage;
//        public DownloadImageTask(ImageView bmImage) {
//            this.bmImage = bmImage;
//        }
//
//
//        protected Bitmap doInBackground(String... urls) {
//            String urldisplay = urls[0];
//            Bitmap mIcon11 = null;
//            try {
//                InputStream in = new java.net.URL(urldisplay).openStream();
//                mIcon11 = BitmapFactory.decodeStream(in);
//            } catch (Exception e) {
//                Log.e("Error", e.getMessage());
//                e.printStackTrace();
//            }
//            return mIcon11;
//        }
//
//        protected void onPostExecute(Bitmap result) {
//            bmImage.setImageBitmap(result);
//        }
//    }
}
