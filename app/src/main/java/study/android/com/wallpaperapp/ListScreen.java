package study.android.com.wallpaperapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.RadioButton;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;

import cz.msebera.android.httpclient.Header;

public class ListScreen extends AppCompatActivity implements View.OnClickListener {

    RadioButton rbtn_newest, rbtn_hotest, rbtn_list, rbtn_table;
    GridView gridView;
    ListView listView;
    int so;
    private String[] strings;
    private OjectItem[] ojectItems = new OjectItem[100];
    private String[] photoUrl = new String[100];
    private String[] description = new String[100];
    private String[] creatime = new String[100];
    private String[] modidiedtime = new String[100];
    private String[] title = new String[100];
    private String[] downloadcount = new String[100];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_screen);
        setupView();
        getdatafromApi(1);
        setClickItem();
    }

    private void getdatafromApi(int information) {
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("registerId", 128);
        params.put("orderBy", information);
        client.get("http://mobilewalls-snapsofts.rhcloud.com/mobilewalls/api/list-walls", params, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(responseString);
                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                        photoUrl[i] = jsonObject1.getString("photoUrl");
                        title[i] = jsonObject1.getString("title");
                        description[i] = jsonObject1.getString("description");
                        downloadcount[i] = "" + jsonObject1.getInt("downloadCount");
                        creatime[i] = "" + jsonObject1.getLong("createdTime");
                        modidiedtime[i] = "" + convertTime(jsonObject1.getLong("modifiedTime"));
                        ojectItems[i] = new OjectItem(title[i], description[i], "" + downloadcount[i]);

                    }
                    so = jsonArray.length();
                    setGridView();
                    setListView();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setupView() {
        rbtn_newest = (RadioButton) findViewById(R.id.btn_newest);
        rbtn_newest.setOnClickListener(this);

        rbtn_hotest = (RadioButton) findViewById(R.id.btn_hotest);
        rbtn_hotest.setOnClickListener(this);

        rbtn_list = (RadioButton) findViewById(R.id.rbtn_list);
        rbtn_list.setOnClickListener(this);

        rbtn_table = (RadioButton) findViewById(R.id.rbtn_table);
        rbtn_table.setOnClickListener(this);

        listView = (ListView) findViewById(R.id.list_view);

        gridView = (GridView) findViewById(R.id.grid_view);

    }

    @Override
    public void onClick(View v) {


        if (v.getId() == R.id.rbtn_list) {
            listView.setVisibility(View.VISIBLE);
            gridView.setVisibility(View.INVISIBLE);
        } else if (v.getId() == R.id.rbtn_table) {
            listView.setVisibility(View.INVISIBLE);
            gridView.setVisibility(View.VISIBLE);
        } else if (v.getId() == R.id.btn_newest)
            getdatafromApi(1);
        else if (v.getId() == R.id.btn_hotest)
            getdatafromApi(3);
    }

    private void setGridView() {
        SetGridAdapter setGridAdapter = new SetGridAdapter(this, R.layout.gridview_adapter, downloadcount, photoUrl, so);
        gridView.setAdapter(setGridAdapter);
    }

    private void setClickItem() {

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(ListScreen.this, DetailScreen.class);
                intent.putExtra("download", downloadcount[position]);
                intent.putExtra("photoUrl", photoUrl[position]);
                intent.putExtra("title", title[position]);
                startActivity(intent);
            }
        });
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(ListScreen.this, DetailScreen.class);
                intent.putExtra("download", downloadcount[position]);
                intent.putExtra("photoUrl", photoUrl[position]);
                intent.putExtra("title", title[position]);
                startActivity(intent);
            }
        });
    }


    private void setListView() {
        //String[] download = getResources().getStringArray(R.array.download);
        //String[] headline = getResources().getStringArray(R.array.headline);
        //String[] title = getResources().getStringArray(R.array.title);

        //       ArrayList<OjectItem> ojectItems1 = new ArrayList<OjectItem>();
        //     ojectItems1.add(new OjectItem(headline[0], title[0], download[0]));
        //   for(int i = 1; i<download.length;i++)
        // {
        //   ojectItems1.add(new OjectItem(headline[i], title[i], download[i]));
        //}
        //for (int i = 0; i <ojectItems.length; i++) {
        //}

        SetListAdapter setListAdapter = new SetListAdapter(this, R.layout.listview_adapter, title, description, downloadcount, modidiedtime, photoUrl, so);
        listView.setAdapter(setListAdapter);
    }

    public String convertTime(long time) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm dd/MM/yyyy");
        return simpleDateFormat.format(time);
    }

}